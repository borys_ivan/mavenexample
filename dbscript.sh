#!/bin/sh
sudo -u postgres psql -U postgres -d postgres -c "create database study00;"
sudo -u postgres psql -U postgres -d postgres -c "create user study with encrypted password 'study';"
sudo -u postgres psql -U postgres -d postgres -c "grant all privileges on database study00 to study;"
