package com.bd;

import javax.persistence.*;

@Entity
@Table(name = "postcode")
public class Postcode {

    @Column(name = "id")
    private Integer id;
    @Column(name = "country", length = 50)
    private String country;
    @Column(name = "region", length = 50)
    private String region;
    @Column(name = "city", length = 50)
    private String city;
    @Column(name = "postcode", length = 50)
    private String postcode;


    @Id
    @SequenceGenerator(name = "my.entity.seq", sequenceName = "EVENT_SEQ", allocationSize = 10, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "my.entity.seq")
    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setregion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Override
    public String toString() {
        return "Postcode{" +
                "country=" + country +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", postcode='" + postcode + "\'}";
    }

}
