package com.app;

import java.io.IOException;
import java.util.List;

import com.bd.HibernateUtil;
import com.bd.Postcode;
import org.hibernate.Session;


public class Insert {

    public static void main(String[] args) throws IOException {

        String city, postCode, country, region;

        List<String[]> allRows = new Read().read();


        try (Session session = HibernateUtil.getSession()) {
            session.beginTransaction();

            System.out.println("Connected to PostgreSQL database!");


            for (String[] row : allRows) {

                Postcode postcode = new Postcode();

                city = row[2];
                postCode = row[1];
                country = row[3];
                region = row[5];


                if (city.equals("")) {
                    city = null;
                }
                if (postCode.equals("")) {
                    postCode = null;
                }
                if (country.equals("")) {
                    country = null;
                }
                if (region.equals("")) {
                    region = null;
                }

                postcode.setCountry(country);
                postcode.setregion(region);
                postcode.setCity(city);
                postcode.setPostcode(postCode);

                session.save(postcode);

            }

            session.getTransaction().commit();

            System.out.println("added data in the table");

        } catch (Throwable cause) {
            cause.printStackTrace();
        }

    }
}

