package com.app;

import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;


public class Read {

    public List<String[]> read() throws IOException {

        String path = new File("").getAbsolutePath() + "/import/GB.txt";

        CSVReader reader = new CSVReader(new FileReader(path),
                '\t', '"',0);

        return reader.readAll();
    }
}
